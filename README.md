# Custom YOLOv5

We are working on a custom version of YOLOv5s (https://github.com/ultralytics/yolov5) for detecting both shapes and characters, and planning to detect also colors in the future.